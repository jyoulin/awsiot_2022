import os
import time
import json
import datetime
import AWSIoTPythonSDK
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import serial
import subprocess    # sys.path.insert(0, '/usr/lib/python2.7/bridge/')
#import socket
import sys
import logging
import getopt

# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print("--------------\n\n")

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

# Read in command-line parameters
host = "xxxxxxxxxxxxxx-ats.iot.us-west-2.amazonaws.com"
rootCAPath = "./aws_7688_test_certifications/root-CA.crt"
certificatePath = "./aws_7688_test_certifications/xxxxxxxxxx-certificate.pem.crt"
privateKeyPath = "./aws_7688_test_certifications/xxxxxxxxxx-private.pem.key"

myAWSIoTMQTTClient = AWSIoTMQTTClient("7688_thing")
myAWSIoTMQTTClient.configureEndpoint(host, 8883)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
print('Connecting AWS MQTT broker...')
myAWSIoTMQTTClient.connect()
print('Connected')

sys.path.insert(0, '/usr/lib/python2.7/bridge/')  
from bridgeclient import BridgeClient as bridgeclient  
value = bridgeclient()

# set id
id = 0
# Publish to the same topic in a loop forever
while True:  
   
    #id = value.get("ID")    
    id += 1
    print("id: " + str(id))
    h0 = value.get("Humidity")
    print("Humidity: " + h0)
    t0 = value.get("Temperature")
    print("Temperature: " + t0)
    light0 = value.get("Light")
    print("Light: " + light0)
    
    # time zone +0
    #t = time.time()
    #date = datetime.datetime.friomtimestamp(t).strftime('%Y%m%d %H%M%S')

    t = time.time()
    now = datetime.datetime.now()
    datetemp = now + datetime.timedelta(hours = 8)    # time zone + 8
    date = datetemp.strftime('%Y%m%d%H%M%S')
    print("Time: " + date)

    myAWSIoTMQTTClient.publish("7688_thing_topic", json.dumps({"ID": id, "Time": date, "Humidity": h0, "Temperature": t0, "Light": light0}), 1)
    time.sleep(10)

print("Stopped")
myAWSIoTMQTTClient.disconnect()


